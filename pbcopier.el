;;; pbcopier.el --- Emacs Interface to pbcopy
;;
;; Copyright (c) 2019 Jade Michael Thornton
;;
;; This file is free software; you can redistribute it and/or modify it under
;; the terms of the GNU General Public License, version 3, as published by the
;; Free Software Foundation.
;;
;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.
;;
;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;;
;; Enables the kill-ring to interact with the system clipboard from within a
;; terminal on macOS, without losing any kill-ring functionality.
;;
;; Activate with:
;;
;;  (turn-on-pbcopier)
;;
;; Pbcopier is based on pbcopy.el by Daniel Nelson, which is in turn based on
;; xclip.el by Leo Shidai Liu.
;;
;;; Code:

(defvar pbcopier-program (executable-find "pbcopy")
	"Name of Pbcopy program tool.")
(defvar pbpaste-program (executable-find "pbpaste")
	"Name of Pbpaste program tool.")

(defvar pbcopier-select-enable-clipboard t
	"Non-nil means cutting and pasting uses the clipboard.
This is in addition to, but in preference to, the primary selection.")

(defvar pbcopier-last-selected-text-clipboard nil
	"The value of the CLIPBOARD X selection from pbcopy.")

(defvar pbcopier-last-selected-text-primary nil
	"The value of the PRIMARY X selection from pbcopy.")

(defun pbcopier-set-selection (type data)
	"TYPE is a symbol: primary, secondary and clipboard.

See `x-set-selection'."
	(when pbcopier-program
		(let* ((process-connection-type nil)
					 (proc (start-process "pbcopy" nil "pbcopy"
																"-selection" (symbol-name type))))
			(process-send-string proc data)
			(process-send-eof proc))))

(defun pbcopier-select-text (text &optional push)
	"See `x-select-text'."
	(pbcopier-set-selection 'primary text)
	(setq pbcopier-last-selected-text-primary text)
	(when pbcopier-select-enable-clipboard
		(pbcopier-set-selection 'clipboard text)
		(setq pbcopier-last-selected-text-clipboard text)))

(defun pbcopier-selection-value ()
	"See `x-cut-buffer-or-selection-value'."
	(when pbcopier-program
		(let (clip-text primary-text)
			(when pbcopier-select-enable-clipboard
				(let ((tramp-mode nil)
							(default-directory "~"))
					(setq clip-text (shell-command-to-string "pbpaste")))
				(setq clip-text
							(cond ;; check clipboard selection
							 ((or (not clip-text) (string= clip-text ""))
								(setq pbcopier-last-selected-text-primary nil))
							 ((eq      clip-text pbcopier-last-selected-text-clipboard) nil)
							 ((string= clip-text pbcopier-last-selected-text-clipboard)
								;; Record the newer string,
								;; so subsequent calls can use the `eq' test.
								(setq pbcopier-last-selected-text-clipboard clip-text)
								nil)
							 (t (setq pbcopier-last-selected-text-clipboard clip-text)))))
			(let ((tramp-mode nil)
						(default-directory "~"))
				(setq primary-text (shell-command-to-string "pbpaste")))
			(setq primary-text
						(cond ;; check primary selection
						 ((or (not primary-text) (string= primary-text ""))
							(setq pbcopier-last-selected-text-primary nil))
						 ((eq      primary-text pbcopier-last-selected-text-primary) nil)
						 ((string= primary-text pbcopier-last-selected-text-primary)
							;; Record the newer string,
							;; so subsequent calls can use the `eq' test.
							(setq pbcopier-last-selected-text-primary primary-text)
							nil)
						 (t (setq pbcopier-last-selected-text-primary primary-text))))
			(or clip-text primary-text))))

;;;###autoload
(defun turn-on-pbcopier ()
	(interactive)
	(setq interprogram-cut-function 'pbcopier-select-text)
	(setq interprogram-paste-function 'pbcopier-selection-value))

;;;###autoload
(defun turn-off-pbcopier ()
	(interactive)
	(setq interprogram-cut-function nil)
	(setq interprogram-paste-function nil))

(add-hook 'terminal-init-xterm-hook 'turn-on-pbcopier)

(provide 'pbcopier)
;;; pbcopier.el ends here
